#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <sys/socket.h>

int main(void){
        struct sockaddr_in direccionServidor;
        direccionServidor.sin_family = AF_INET;
        direccionServidor.sin_addr.s_addr = inet_addr("172.17.0.2");
        direccionServidor.sin_port = htons(8080);

        int cliente =  socket(AF_INET, SOCK_STREAM, 0);

        if(connect(cliente, (sockaddr *) & direccionServidor, sizeof(direccionServidor)) != 0){
                perror("No se puedo conectar");
                return 1;
        }

        char* buffer = (char*) malloc(1000);

        int bytesRecibidos = recv(cliente, buffer, 1000, 0);
        buffer[bytesRecibidos] = '\0';
        printf("La fecha es >> %s\n", buffer);
        //send(cliente, mensaje, strlen(mensaje), 0);

        free(buffer);

        return 0;

}
