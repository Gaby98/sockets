#include <stdio.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

int main(void){
        struct sockaddr_in direccionServidor;
        direccionServidor.sin_family = AF_INET;
        direccionServidor.sin_addr.s_addr = INADDR_ANY;
        direccionServidor.sin_port = htons(8080);

        int servidor = socket(AF_INET, SOCK_STREAM, 0);

        int activado = 1;
        setsockopt(servidor, SOL_SOCKET, SO_REUSEADDR, & activado, sizeof(activado));

        if ( bind (servidor,(sockaddr *) & direccionServidor, sizeof (direccionServidor)) != 0){
                perror("Fallo el bind");
                return 1;
        }

        printf("Servidor activo\n");
        listen(servidor, 100);

        //--------------------------

        struct sockaddr_in direccionCliente;
		unsigned int tamanoDireccion;
        int cliente = accept(servidor, (sockaddr * ) &direccionCliente, &tamanoDireccion);

        time_t t;
        char fechayhora[100];
        struct tm *tm;

        t = time(NULL);
        tm = localtime(&t);
        strftime(fechayhora, 100, "%d/%m/%Y", tm);

        printf("Se recibio a un cliente >> %d!!\n" , cliente);

        send(cliente, fechayhora, sizeof(fechayhora), 0);

        //--------------------------

        char* buffer = (char*) malloc(1000);

        while(1){
                int bytesRecibidos = recv(cliente, buffer, 1000, 0);
                if(bytesRecibidos <= 0 ){
                        perror("El cliente se desconecto");
				}

                buffer[bytesRecibidos] = '\0';
                printf("Mensaje del cliente: %s\n >>", buffer);
        }

        free(buffer);
                                                                                                                                                                                  
	return 0;   

}